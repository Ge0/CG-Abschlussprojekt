#ifndef FLAG_H
#define FLAG_H

#include <vector>
#include "constraint.h"
#include "globject.h"
#include "vertex.h"


class Flag : public GLObject
{
public:
  Flag(std::string name, const QString &texture, QOpenGLShaderProgram *shader,
       Model *model, bool fix = true, unsigned int neighDepths = 1,std::vector<double> position = {.0,.0,.0},
       std::vector<double> scale = {1.0,1.0,1.0},
       std::vector<double> rotation = {.0,.0,.0});

  void fixVert(int vertp);
  void setNeighDepth(unsigned int depth);
  void granuUp(unsigned int side);
  void blow(Vec3 wind, bool takeNormals = true);
  void update();



protected:

private:
  std::vector<Vertex*> verts;
  std::vector<Constraint*> constraints;
  unsigned int neighDepths;

  std::vector<Vertex *> getDirectNeigh(int pos);

  bool fixed = true;

  void writeVBOback();
  void makeConstraint(Vertex *v1, Vertex *v2);
};

#endif // FLAG_H
