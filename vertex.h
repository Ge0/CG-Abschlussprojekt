#ifndef VERTEX_H
#define VERTEX_H
#include <vector>
#include <iostream>
#include "vec3.h"

class Vertex
{
public:
  Vertex();
  Vertex(Vec3 pos, Vec3 norm, Vec3 tex, Vec3 objectpos);
  bool movable;
  float mass;
  //4th coordinate is always 1
  Vec3 pos;
  Vec3 oldpos;
  Vec3 acceleration;
  Vec3 textcoord;
  Vec3 normal;
  Vec3 realPos;

  std::vector<Vertex*> directNeighbours;
  std::vector<std::vector<Vertex *>> neighmap;

  bool used = false;

  void addForce(Vec3 force, unsigned int depth);
  void move();
  void offsetPos(Vec3 v);
  void addToNormal(Vec3 norm);
  void makeNeighMap();

  friend std::ostream& operator<<(std::ostream& os, Vertex* vert);

};

#endif // VERTEX_H
