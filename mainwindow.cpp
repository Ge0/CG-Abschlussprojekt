#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  timer = new QTimer();
  ui->setupUi(this);
  connect(ui->ballpossld,SIGNAL(valueChanged(int)),
          ui->widget, SLOT(receiveBallPos(int)));
  connect(ui->sceneSelector,SIGNAL(valueChanged(int)),
          ui->widget, SLOT(receiveScene(int)));
  connect(ui->depthCounter,SIGNAL(valueChanged(int)),
          ui->widget, SLOT(receiveDepth(int)));
  connect(ui->meshGranu, SIGNAL(valueChanged(int)),
          ui->widget, SLOT(receiveGranu(int)));
  connect(ui->widget,SIGNAL(sendFPS(double)),
          ui->fpscounter, SLOT(display(double)));
  connect(timer,SIGNAL(timeout()),ui->widget,SLOT(receiveForce()));
      timer->start(1000/60);

}

MainWindow::~MainWindow()
{
  delete ui;
}
