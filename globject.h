#ifndef GLOBJECT_H
#define GLOBJECT_H
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <string>
#include <vector>
#include "model.h"

#define PI 3.14159265
#define CONVERSIONDEGRAD PI/180

class GLObject
{
public:
  GLObject(std::string name, std::vector<double> position,
           std::vector<double> rotation, std::vector<double> scale,
           const QString &texture, QOpenGLShaderProgram *shader, Model *model);

  //getter
  std::string getName();
  std::vector<double> getScale();
  std::vector<double> getPosition();
  std::vector<double> getRotation();
  QOpenGLTexture *getTexture();
  QOpenGLShaderProgram *getShader();
  std::vector<GLObject *> getChildren();
  Model *getModel();
  //setter
  void setName(std::string name);
  void setScale(double x, double y, double z);
  void setPostition(double x, double y, double z);
  void setRotation(double x, double y, double z);
  void setTexture(const QString &texture);
  void setShader (QOpenGLShaderProgram *shader);
  void addChildren(GLObject* child);
  void setModel(Model *model);

  virtual void update() = 0;
  void render(QMatrix4x4 m, QMatrix4x4 p, QMatrix4x4 v, float time = 0.0f);
protected:
  GLObject();
  Model *model;
private:


  std::string name;
  std::vector<double> scale;
  std::vector<double> position;
  std::vector<double> rotation;

  QOpenGLTexture *texture;
  QOpenGLShaderProgram *shader;
  std::vector<GLObject *> children;
};

#endif // GLOBJECT_H
