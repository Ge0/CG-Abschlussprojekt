#version 130
// default130.vert: a simple vertex shader

uniform mat4 modelMatrix;
uniform mat4 perspectiveMatrix;
uniform mat4 viewMatrix;
attribute vec4 normCoord;
attribute vec4 vert;
attribute vec4 texCoord;
varying vec4 texC;
varying vec4 norC;

void main()
{
  texC = texCoord;
  norC = normCoord;
  gl_Position = perspectiveMatrix * viewMatrix * modelMatrix * vert;
}
