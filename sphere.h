#ifndef SPHERE_H
#define SPHERE_H

#include "globject.h"



class Sphere : public GLObject
{
public:
  Sphere(Model *model, QOpenGLShaderProgram *shader,const QString &texture,
         std::string name, double size = 1,  double distx = 0, double disty = 0, double distz = 0);
  void update();
protected:
private:
  Sphere();

  std::vector<double> gravitation = {0,9.81,0};
  std::vector<double> force = {0,0,0};

};

#endif // SPHERE_H
