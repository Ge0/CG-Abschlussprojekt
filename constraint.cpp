#include "constraint.h"
#include <math.h>

Constraint::Constraint(Vertex *v1, Vertex *v2) : v1(v1),v2(v2)
{
  Vec3 vec = v1->pos - v2->pos;
  this->rest_distance = vec.length();
}

void Constraint::satisfyConstraint()
{

  Vec3 v1tov2 = v2->pos - v1->pos;
  float currentDist = v1tov2.length();
  Vec3 correction = v1tov2 * (1 - this->rest_distance/currentDist);
  Vec3 corrHalf = correction * 0.5;
  v1->offsetPos(corrHalf);
  v2->offsetPos(-corrHalf);
}
