#-------------------------------------------------
#
# Project created by QtCreator 2017-06-03T11:12:17
#
#-------------------------------------------------

QT       += core gui opengl

unix: LIBS += -lassimp
# Windows can't deal with assimp as a system library for some reason
#win32: LIBS += -LC:/Path/to/assimp/code/ -lassimp
#win32: INCLUDEPATH += C:/Path/to/assimp/include
#win32: DEPENDPATH += C:/Path/to/assimp/include

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Abschlussprojekt
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    myglwidget.cpp \
    globject.cpp \
    model.cpp \
    sphere.cpp \
    modelloader.cpp \
    flag.cpp \
    vertex.cpp \
    vec3.cpp \
    constraint.cpp

HEADERS  += mainwindow.h \
    myglwidget.h \
    globject.h \
    model.h \
    sphere.h \
    modelloader.h \
    flag.h \
    vertex.h \
    vec3.h \
    constraint.h

FORMS    += mainwindow.ui

RESOURCES += \
    ress.qrc
