#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H


#include "globject.h"
#include "vec3.h"

#include <QOpenGLDebugMessage>
#include <QOpenGLWidget>
#include <QKeyEvent>
#include <QWidget>
#include <vector>
#include <QTime>

class MyGLWidget : public QOpenGLWidget
{
  Q_OBJECT

public:
  MyGLWidget();
  MyGLWidget(QWidget *parent);

  static Vec3 windDir;
  static Vec3 ballPos;

public slots:
  void onMessageLogged(QOpenGLDebugMessage message);
  void receiveGranu(int value);
  void receiveDepth(int value);
  void receiveScene(int value);
  void receiveBallPos(int value);
  void receiveForce();

  // QOpenGLWidget interface
protected:
  void initializeGL();
  void resizeGL(int width, int height);
  void paintGL();
  void wheelEvent(QWheelEvent *event);
  void mousePressEvent(QMouseEvent * event);
  void mouseReleaseEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
  void keyPressEvent(QKeyEvent *event);

private:
  QOpenGLDebugLogger *debugLogger;
  std::vector<GLObject *> objects;
  std::vector<GLObject *> boden;

  //camera
  double camDistance = -20;
  double camRotX = 0;
  double camRotY = 0;
  double maxCamRot = 90;

  //mouse events
  double mousePosX;
  double mousePosY;
  bool mouseLeftPressed = false;

  //fps counter
  QTime time;
  unsigned int frames = 0;
  double fps = 0;

  //generelle geladene objekte
  QOpenGLShaderProgram *default130, *phong, *normal;
  bool alreadyLoaded = false;
  void loadObjects(int scene);
  void updateFPS(int ms);

  void loadScene(int scene);

  int sceneval = 0;

signals:
  void sendFPS(double);

};
#endif // MYGLWIDGET_H
