#ifndef MODEL_H
#define MODEL_H

#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>



class Model
{
public:
  Model();
  Model(const std::string& pFile);
  ~Model(void);
  //getter
  bool HasTexCoord();
  unsigned int getVbolength();
  unsigned int getIbolength();
  GLfloat *getVboData();
  GLuint *getIndexData();
  QOpenGLBuffer *getVbo();
  QOpenGLBuffer *getIbo();
  QOpenGLVertexArrayObject *getVao();
  //setter
  void setHasTexCoord(bool hasTexCoord);
  void setVbolength(unsigned int vbolength);
  void setIbolength(unsigned int ibolength);
  void setVboData(GLfloat *vboData);
  void setIndexData(GLuint *indexData);

  void setVbo(QOpenGLBuffer *vbo);
  void setIbo(QOpenGLBuffer *ibo);
  void setVao(QOpenGLVertexArrayObject *vao);

  void resetBuffers();

protected:
private:
  bool hasTexCoord;
  unsigned int vboLength;
  unsigned int iboLength;
  GLfloat *vboData;
  GLuint *indexData;

  QOpenGLBuffer *vbo = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  QOpenGLBuffer *ibo = new QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
  QOpenGLVertexArrayObject *vao = new QOpenGLVertexArrayObject();
};

#endif // MODEL_H
