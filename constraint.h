#ifndef CONSTRAINT_H
#define CONSTRAINT_H

#include "vertex.h"



class Constraint
{
public:
  Constraint(Vertex *v1, Vertex *v2);
  Vertex *v1, *v2;

  void satisfyConstraint();

private:
  float rest_distance;

};

#endif // CONSTRAINT_H
