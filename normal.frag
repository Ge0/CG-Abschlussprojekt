#version 130
// default130. frag : a simple fragment shader
varying vec4 texC;
varying vec4 norC;
uniform sampler2D texture;
void main()
{
  gl_FragColor = norC;
}
