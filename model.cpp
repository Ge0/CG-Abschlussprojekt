#include "model.h"
#include "modelloader.h"

Model::Model()
{

}

Model::Model(const std::string &pFile)
{
  ModelLoader model;
  //lade Modell aus Datei
  bool res = model.loadObjectFromFile(pFile);
  // Wenn erfolgreich, generiere VBO und Index-Array
  if (res) {
    //check für normalen
    if(model.hasTextureCoordinates()){
      this->hasTexCoord = true;
      this->vboLength = model.lengthOfVBO(0,true,true);
      this->iboLength = model.lengthOfIndexArray();
      this->vboData = new GLfloat[this->vboLength];
      this->indexData = new GLuint[this->iboLength];
      model.genVBO(this->vboData,0,true,true);
      model.genIndexArray(this->indexData);
      }
    else{
      this->hasTexCoord = false;
      this->vboLength = model.lengthOfVBO(0,true,false);
      this->iboLength = model.lengthOfIndexArray();
      this->vboData = new GLfloat[this->vboLength];
      this->indexData = new GLuint[this->iboLength];
      model.genVBO(this->vboData,0,true,false);
      model.genIndexArray(this->indexData);
    }
  }
  else {}
  this->vao->create();
  this->vao->bind();
  this->vbo->create();
  this->vbo->bind();
  this->vbo->setUsagePattern(QOpenGLBuffer::DynamicDraw);
  this->vbo->allocate(this->vboData, sizeof(GLfloat) * this->vboLength);
  this->vbo->release();

  this->ibo->create();
  this->ibo->bind();
  this->ibo->setUsagePattern(QOpenGLBuffer::DynamicDraw);
  this->ibo->allocate(this->indexData, sizeof(GLuint) * this->iboLength);
  this->ibo->release();
  this->vao->release();
}

Model::~Model(void)
{
  this->vbo->destroy();
  this->ibo->destroy();
  this->vao->destroy();
  delete(this->vbo);
  delete(this->ibo);
  delete(this->vao);

  delete(this->vboData);
  delete(this->indexData);
}

bool Model::HasTexCoord()
{
  return this->hasTexCoord;
}

unsigned int Model::getVbolength()
{
  return this->vboLength;
}

unsigned int Model::getIbolength()
{
  return this->iboLength;
}

GLfloat *Model::getVboData()
{
  return this->vboData;
}

GLuint *Model::getIndexData()
{
  return this->indexData;
}

QOpenGLBuffer *Model::getVbo()
{
  return this->vbo;
}

QOpenGLBuffer *Model::getIbo()
{
  return this->ibo;
}

QOpenGLVertexArrayObject *Model::getVao()
{
  return this->vao;
}

void Model::setHasTexCoord(bool hasTexCoord)
{
  this->hasTexCoord = hasTexCoord;
}

void Model::setVbolength(unsigned int vbolength)
{
  this->vboLength = vbolength;
}

void Model::setIbolength(unsigned int ibolength)
{
  this->iboLength = ibolength;
}

void Model::setVboData(GLfloat *vboData)
{
  this->vboData = vboData;
}

void Model::setIndexData(GLuint *indexData)
{
  this->indexData = indexData;
}

void Model::setVbo(QOpenGLBuffer *vbo)
{
  this->vbo = vbo;
}

void Model::setIbo(QOpenGLBuffer *ibo)
{
  this->ibo = ibo;
}

void Model::setVao(QOpenGLVertexArrayObject *vao)
{
  this->vao = vao;
}

void Model::resetBuffers()
{
  this->vbo->destroy();
  this->ibo->destroy();
  this->vao->destroy();

  this->vao->create();
  this->vao->bind();
  this->vbo->create();
  this->vbo->bind();
  this->vbo->setUsagePattern(QOpenGLBuffer::DynamicDraw);
  this->vbo->allocate(this->vboData, sizeof(GLfloat) * this->vboLength);
  this->vbo->release();

  this->ibo->create();
  this->ibo->bind();
  this->ibo->setUsagePattern(QOpenGLBuffer::DynamicDraw);
  this->ibo->allocate(this->indexData, sizeof(GLuint) * this->iboLength);
  this->ibo->release();
  this->vao->release();
}
