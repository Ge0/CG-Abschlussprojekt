#include "flag.h"
#include "vec3.h"
#include <QtOpenGL>
#include "myglwidget.h"
#include <math.h>

Flag::Flag(std::string name, const QString &texture,
           QOpenGLShaderProgram *shader, Model *model, bool fix, unsigned int neighDepths,
           std::vector<double> position, std::vector<double> scale,
           std::vector<double> rotation) :
  GLObject(name,position,rotation,scale,texture,shader,model)
{
  this->fixed = fix;
  this->granuUp(15);
  this->setNeighDepth(neighDepths);
}

void Flag::fixVert(int vertp)
{
  this->verts[vertp]->movable =  !this->verts[vertp]->movable;
}

void Flag::setNeighDepth(unsigned int depth)
{
  this->neighDepths = depth;
}

void Flag::update()
{
  if((MyGLWidget::ballPos - Vec3(this->getPosition()[0],this->getPosition()[1],this->getPosition()[1])).length() < 3){
    foreach (Vertex *v, this->verts) {
      //get vect between ball and vert -> will be direction of force
      Vec3 btoV = (v->realPos + v->pos * this->getScale()[1]) - MyGLWidget::ballPos;
      if(btoV.length() <= 1.1){
        v->offsetPos(btoV*(1.1 - btoV.length()));
      }
    }
  }
}

std::vector<Vertex *> Flag::getDirectNeigh(int pos)
{
  std::vector<Vertex *> neighs;
  int up = pos + sqrt(this->verts.size()),
      down = pos - sqrt(this->verts.size()),
      left = pos-1,
      right = pos+1,
      upl = up-1,
      upr = up +1,
      downl = down -1,
      downr = down +1;
  //up
  if(up < (int) this->verts.size()){
    neighs.push_back(this->verts[up]);
  }
  //upright
  if(upr % (int)sqrt(this->verts.size()) != 0 && upr < (int) this->verts.size()){
    neighs.push_back(this->verts[upr]);
  }
  //right
  if(right%this->verts.size() != 0){
    neighs.push_back(this->verts[right]);
  }
  //downright
  if(downr > 0 && downr % (int) sqrt(this->verts.size()) != 0){
    neighs.push_back(this->verts[downr]);
  }
  //down
  if(down >= 0){
    neighs.push_back(this->verts[down]);
  }
  //downleft
  if(downl > 0 && pos % (int) sqrt(this->verts.size()) != 0){
    neighs.push_back(this->verts[downl]);
  }
  //left
  if((pos % this->verts.size()) != 0){
    neighs.push_back(this->verts[left]);
  }
  //upleft
  if(pos % (int)sqrt(this->verts.size()) != 0 && upl < (int) this->verts.size()){
    neighs.push_back(this->verts[upl]);
  }

  return neighs;
}

void Flag::writeVBOback()
{
  unsigned int offset = 8;
  if (this->getModel()->HasTexCoord())
    offset = 12;
  GLfloat *vbodat = this->getModel()->getVboData();
  for(unsigned int i = 0;i< this->getModel()->getVbolength();i += offset){
    vbodat[i+0] = this->verts[i/offset]->pos.f[0];
    vbodat[i+1] = this->verts[i/offset]->pos.f[1];
    vbodat[i+2] = this->verts[i/offset]->pos.f[2];
    vbodat[i+3] = 1;

    vbodat[i+4] = this->verts[i/offset]->normal.f[0];
    vbodat[i+5] = this->verts[i/offset]->normal.f[1];
    vbodat[i+6] = this->verts[i/offset]->normal.f[2];
    vbodat[i+7] = 1;

    if(offset == 12){
      vbodat[i+8]  = this->verts[i/offset]->textcoord.f[0];
      vbodat[i+9]  = this->verts[i/offset]->textcoord.f[1];
      vbodat[i+10] = this->verts[i/offset]->textcoord.f[2];
      vbodat[i+11] = 1;
    }
  }
}

void Flag::makeConstraint(Vertex *v1, Vertex *v2)
{
  this->constraints.push_back(new Constraint(v1,v2));
}

void Flag::granuUp(unsigned int side)
{

  //->delete buffers in model destructor
  delete(this->model);
  this->model = new Model();
  unsigned int vertx  = pow(side,2);
  //calculate index length
  unsigned int idexlen = pow(side-1,2)*6;
  this->model->setIbolength(idexlen);
  this->model->setHasTexCoord(true);
  int offset = 12;//coords,norms,texts
  this->model->setVbolength(vertx*offset);

  //create vertex buffer data
  //delete old vertices
  foreach (Vertex *v, this->verts) {
    delete(v);
  }
  //delete verts vector
  this->verts.clear();
  //create new vertices
  for(unsigned int i = 0; i < vertx;i++){
    this->verts.push_back(new Vertex(Vec3((i%side)/(float)side,(i/side)/(float)side,0),
                                     Vec3(0,0,1),
                                     Vec3((i%side)/(float)side,(i/side)/(float)side,0),
                                     Vec3(this->getPosition()[0],this->getPosition()[1],this->getPosition()[2])));
  }

  GLfloat *vboData = new GLfloat[this->model->getVbolength()];

  for(unsigned int i = 0;i< this->getModel()->getVbolength();i += offset){
    vboData[i+0] = this->verts[i/offset]->pos.f[0];
    vboData[i+1] = this->verts[i/offset]->pos.f[1];
    vboData[i+2] = this->verts[i/offset]->pos.f[2];
    vboData[i+3] = 1;

    vboData[i+4] = 0;
    vboData[i+5] = 1;
    vboData[i+6] = 0;
    vboData[i+7] = 1;

    vboData[i+8] = this->verts[i/offset]->textcoord.f[0];
    vboData[i+9] = this->verts[i/offset]->textcoord.f[1];
    vboData[i+10] = this->verts[i/offset]->textcoord.f[2];
    vboData[i+11] = 1;
  }

  this->model->setVboData(vboData);
  //create indexbuffer data
  GLuint *indexData = new GLuint[this->model->getIbolength()];

  int j = 0;
  for(unsigned int i = 0; i < vertx; i++){
    //if at the top or right side of the square -> create indexes
    if((i +1) % side !=0 && i + side < vertx){
      //lower triangle
      indexData[j] = i;
      j++;
      indexData[j] = i+1;
      j++;
      indexData[j] = i+1+side;
      j++;
      //upper triangle
      indexData[j] = i;
      j++;
      indexData[j] = i+1+side;
      j++;
      indexData[j] = i+side;
      j++;
    }
  }

  this->model->setIndexData(indexData);
  //create constraints
  //delete old constraints
  foreach (Constraint *c, this->constraints) {
    delete(c);
  }
  this->constraints.clear();
  //create new ones
  for(unsigned int i = 0; i < this->verts.size(); i++){
    //calc adjacent vertexes
    //i = pos of v1
    // |
    unsigned int up = i + sqrt(this->verts.size());
    // _
    unsigned int right = i+1;
    // /
    unsigned int upright = up + 1;
  if(up < this->verts.size()){
    this->makeConstraint(this->verts[i],this->verts[up]);
  }
  if((right)% (int) sqrt(this->verts.size()) != 0){
    this->makeConstraint(this->verts[i],this->verts[right]);
  }
  if(upright % (int)sqrt(this->verts.size()) != 0 && upright < this->verts.size()){
    this->makeConstraint(this->verts[i],this->verts[upright]);
  }
  //    if uptoright
  if(up < this->verts.size() && (right)% (int) sqrt(this->verts.size()) != 0){
    this->makeConstraint(this->verts[up],this->verts[right]);
  }
}

  //directe nachbarns geben
  for(unsigned int i  = 0; i < this->verts.size(); i++){
    foreach (Vertex *v, this->getDirectNeigh(i)) {
      this->verts[i]->directNeighbours.push_back(v);
    }
  }
  this->model->resetBuffers();
}

void Flag::blow(Vec3 wind, bool takeNormals)
{
  Vec3 force = wind;
  for (unsigned int i = 0; i < this->verts.size();i++){
    if(takeNormals){
      //wind vektor multipliziert mit skalarprodukt zwichen normalen und wind
      Vec3 normie = this->verts[i]->normal.normalized();
      force = wind * abs(/*this->verts[i]->normal*/normie.dot(wind));
    }
    //normale resetten, wird neu berechnet
    this->verts[i]->normal = Vec3(0,0,0);
    this->verts[i]->addForce(force, this->neighDepths);
  }

  for(int i = 0;i< 30 ; i++){
    foreach(Constraint *constr, this->constraints){
      constr->satisfyConstraint();
    }
  }

  for (unsigned int i = 0; i < this->verts.size();i++){
  //normalen neu berechnen
  unsigned int up = i + sqrt(this->verts.size());
  // _
  unsigned int right = i+1;
  // /
  unsigned int upright = up + 1;

    if(up < this->verts.size() && (right)% (int) sqrt(this->verts.size()) != 0){
      Vec3 v1 = this->verts[i]->pos - this->verts[right]->pos;
      Vec3 v2 = this->verts[up]->pos - this->verts[right]->pos;
      Vec3 norm = v1.cross(v2);

      this->verts[i]->addToNormal(norm);
      this->verts[right]->addToNormal(norm);
      this->verts[up]->addToNormal(norm);

      v1 = this->verts[right]->pos - this->verts[upright]->pos;
      v2 = this->verts[up]->pos - this->verts[upright]->pos;
      norm = v1.cross(v2);

      this->verts[upright]->addToNormal(norm);
      this->verts[right]->addToNormal(norm);
      this->verts[up]->addToNormal(norm);
    }
    this->verts[i]->move();
  }

  this->writeVBOback();
  this->getModel()->resetBuffers();
}
