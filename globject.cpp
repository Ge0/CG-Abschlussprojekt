#include "globject.h"

GLObject::GLObject()
{
  //disabled - set as private
}

GLObject::GLObject(std::string name, std::vector<double> position,
                   std::vector<double> rotation, std::vector<double> scale,
                   const QString &texture, QOpenGLShaderProgram *shader, Model *model)
{
  this->name = name;
  this->position = position;
  this->rotation = rotation;
  this->scale = scale;
  this->setTexture(texture);
  this->setShader(shader);
  this->setModel(model);
}

std::string GLObject::getName()
{
  return this->name;
}

std::vector<double> GLObject::getScale()
{
  return this->scale;
}

std::vector<double> GLObject::getPosition()
{
  return this->position;
}

std::vector<double> GLObject::getRotation()
{
  return this->rotation;
}

QOpenGLTexture *GLObject::getTexture()
{
  return this->texture;
}

QOpenGLShaderProgram *GLObject::getShader()
{
  return this->shader;
}

std::vector<GLObject *> GLObject::getChildren()
{
  return this->children;
}

Model *GLObject::getModel()
{
  return this->model;
}

void GLObject::setName(std::string name)
{
  this->name = name;
}

void GLObject::setScale(double x, double y, double z)
{
  this->scale.resize(3);
  this->scale[0] = x;
  this->scale[1] = y;
  this->scale[2] = z;
}

void GLObject::setPostition(double x, double y, double z)
{
  this->position.resize(3);
  this->position[0] = x;
  this->position[1] = y;
  this->position[2] = z;
}

void GLObject::setRotation(double x, double y, double z)
{
  this->rotation.resize(3);
  this->rotation[0] = x;
  this->rotation[1] = y;
  this->rotation[2] = z;
}

void GLObject::setTexture(const QString &texture)
{

  this->texture = new QOpenGLTexture(QImage(texture).mirrored());
  this->texture->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
  this->texture->setMagnificationFilter(QOpenGLTexture::Linear);
}

void GLObject::setShader(QOpenGLShaderProgram *shader)
{
  this->shader = shader;
}

void GLObject::addChildren(GLObject *child)
{
  this->children.push_back(child);
}

void GLObject::setModel(Model *model)
{
  this->model = model;
}

void GLObject::render(QMatrix4x4 m, QMatrix4x4 p, QMatrix4x4 v,float time)
{
  QMatrix3x3 n = m.normalMatrix();
  QVector4D lightPosition(500.0,500.0,500.0,1.0);
  QVector3D lightIntensity(1.5,1.5,1.5);
  QVector3D kd(1.0,1.0,1.0);
  QVector3D ka(0.1,0.1,0.1);
  QVector3D ks(1.0,1.0,1.0);
  float shininess = 32.0;

  this->getShader()->bind();
  this->getModel()->getVao()->bind();
  this->getModel()->getVbo()->bind();
  this->getModel()->getIbo()->bind();


  int attrVertices = 0;
  attrVertices = this->getShader()->attributeLocation("vert");
  int attrTexCoords = 1;
  attrTexCoords = this->getShader()->attributeLocation("texCoord");
  int attrNorCoords = 2;
  attrNorCoords = this->getShader()->attributeLocation("normCoord");

  this->getShader()->enableAttributeArray(attrVertices);
  if(this->getModel()->HasTexCoord())
    this->getShader()->enableAttributeArray(attrTexCoords);
  this->getShader()->enableAttributeArray(attrNorCoords);

  this->getTexture()->bind();

  int unifMatrix = 0;
  unifMatrix = this->getShader()->uniformLocation("modelMatrix");
  this->getShader()->setUniformValue(unifMatrix,m);

  int unifMatrix1 = 1;
  unifMatrix1 = this->getShader()->uniformLocation("perspectiveMatrix");
  this->getShader()->setUniformValue(unifMatrix1,p);

  int unifMatrix2 = 2;
  unifMatrix = this->getShader()->uniformLocation("viewMatrix");
  this->getShader()->setUniformValue(unifMatrix2,v);

  int unifTime = 3;
  unifTime = this->getShader()->uniformLocation("time");
  this->getShader()->setUniformValue(unifTime,time);

  int unifMatrix3 = 4;
  unifMatrix3 = this->getShader()->uniformLocation("normalMatrix");
  this->getShader()->setUniformValue(unifMatrix3, n);

  int unifLightP = 5;
  unifLightP = this->getShader()->uniformLocation("lightPosition");
  this->getShader()->setUniformValue(unifLightP, lightPosition);

  int unifLightI = 6;
  unifLightI = this->getShader()->uniformLocation("lightIntensity");
  this->getShader()->setUniformValue(unifLightI, lightIntensity);

  int unifKd = 7;
  unifKd = this->getShader()->uniformLocation("kd");
  this->getShader()->setUniformValue(unifKd, kd);

  int unifKa = 8;
  unifKa = this->getShader()->uniformLocation("ka");
  this->getShader()->setUniformValue(unifKa, ka);

  int unifKs = 9;
  unifKs = this->getShader()->uniformLocation("ks");
  this->getShader()->setUniformValue(unifKs, ks);

  int unifShine = 10;
  unifShine = this->getShader()->uniformLocation("shininess");
  this->getShader()->setUniformValue(unifShine, shininess);

  if(this->getModel()->HasTexCoord()){
    int offset = 0;
    int stride = 12 * sizeof(GLfloat);
    this->getShader()->setAttributeBuffer(attrVertices,GL_FLOAT,offset,4,stride);
    offset += 4 * sizeof(GLfloat);
    this->getShader()->setAttributeBuffer(attrNorCoords,GL_FLOAT,offset,4,stride);
    offset += 4 * sizeof(GLfloat);
    this->getShader()->setAttributeBuffer(attrTexCoords,GL_FLOAT,offset,4,stride);
  }
  else{
    int offset = 0;
    int stride = 8 * sizeof(GLfloat);
    this->getShader()->setAttributeBuffer(attrVertices,GL_FLOAT,offset,4,stride);
    offset += 4 * sizeof(GLfloat);
    this->getShader()->setAttributeBuffer(attrTexCoords,GL_FLOAT,offset,4,stride);
  }

  glDrawElements(GL_TRIANGLES,this->getModel()->getIbolength(),GL_UNSIGNED_INT,0);

  this->getShader()->disableAttributeArray(attrVertices);
  if(this->getModel()->HasTexCoord())
    this->getShader()->disableAttributeArray(attrTexCoords);
  this->getShader()->disableAttributeArray(attrNorCoords);

  this->getTexture()->release();
  this->getModel()->getVbo()->release();
  this->getModel()->getIbo()->release();
  this->getModel()->getVao()->release();
  this->getShader()->release();
}
