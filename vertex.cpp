#include "vertex.h"
#include <math.h>

Vertex::Vertex()
{

}

Vertex::Vertex(Vec3 pos, Vec3 norm, Vec3 tex, Vec3 objectpos) : movable(true), textcoord(tex)
{
  this->normal = norm;
  this->mass = 1;
  this->pos = pos;
  this->oldpos = pos;
  this->realPos = objectpos;
  this->acceleration = Vec3(0,0,0);

}

void Vertex::addForce(Vec3 force, unsigned int depth)
{

  if(depth > 0 && this->movable == true){

  if(this->used == false){
    this->acceleration += force/this->mass;
    this->used = true;
  }
  for(unsigned int i = 0; i < this->directNeighbours.size(); i++)
  {
    if(this->directNeighbours[i]->used == false){
      this->directNeighbours[i]->acceleration += (force*0.5)/this->mass;
      this->directNeighbours[i]->used = true;
    }
  }
  for(unsigned int i = 0; i < this->directNeighbours.size(); i++){
    this->directNeighbours[i]->addForce(force * 0.5, depth -1);
  }
  //end
  this->used = false;
  for(unsigned int i = 0; i < this->directNeighbours.size(); i++){
    this->directNeighbours[i]->used = false;
  }
  }
}

void Vertex::move()
{
  if(movable){
    Vec3 temp = this->pos;
    this->pos = (this->pos*2 - this->oldpos) + acceleration;
    this->oldpos = temp;
  }
  this->acceleration = Vec3(0,0,0);
}

void Vertex::offsetPos(Vec3 v)
{
  if(this->movable)
    this->pos += v;
}

void Vertex::addToNormal(Vec3 norm)
{
  this->normal += norm.normalized();
}

void Vertex::makeNeighMap()
{
//  std::vector<Vertex *> tmp;
//  for(int i = 0; i < this->directNeighbours.size(); i++){

//    }
}



std::ostream& operator<<(std::ostream& os, Vertex* vert)
{
    os << "x:" << vert->pos.f[0] << "\ty:" << vert->pos.f[1] << "\tz:" << vert->pos.f[2];
    return os;
}
