#include "modelloader.h"
#include "myglwidget.h"
#include "sphere.h"
#include "flag.h"

#include <QMatrix4x4>
#include <iostream>
#include <math.h>

Vec3 MyGLWidget::windDir = Vec3(0.0,0.01,0.01);
Vec3 MyGLWidget::ballPos = Vec3(500.0,500.01,500.01);

MyGLWidget::MyGLWidget()
{
  setFocusPolicy(Qt::StrongFocus);
}

MyGLWidget::MyGLWidget(QWidget *parent) : QOpenGLWidget(parent)
{
  setFocusPolicy(Qt::StrongFocus);
}

void MyGLWidget::initializeGL()
{
  this->debugLogger = new QOpenGLDebugLogger(this); // this is a member variable
  connect(this->debugLogger, SIGNAL(messageLogged(QOpenGLDebugMessage)), this,
  SLOT(onMessageLogged(QOpenGLDebugMessage)), Qt::DirectConnection);
  if (this->debugLogger->initialize()) {
    this->debugLogger->startLogging(QOpenGLDebugLogger::SynchronousLogging);
    this->debugLogger->enableMessages();
  }

  this->time.start();
  this->loadObjects(0);

  this->maxCamRot = 75;

  glEnable(GL_DEPTH_TEST);
  glCullFace(GL_BACK);
  glEnable(GL_CULL_FACE);
  glDepthFunc(GL_LEQUAL);

  glClearDepth(1.0f);
  glClearColor(0.37647058823 , 0.62745098039, 1, 1);
}

void MyGLWidget::resizeGL(int width, int height)
{
  // Compute aspect ratio
  height = (height == 0) ? 1 : height;
  // Set viewport to cover the whole window
  glViewport(0, 0, width, height);
}

void MyGLWidget::paintGL()
{
  // Clear buffer to set color and alpha
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  QMatrix4x4 modelMatrix,perspectiveMatrix, viewMatrix;
  //set perspective
  perspectiveMatrix.setToIdentity();
  perspectiveMatrix.perspective(45.0,4.0/3.0,0.1,100000.0);

  //set view
  viewMatrix.setToIdentity();
  QMatrix4x4 cameraTransformation;
  cameraTransformation.rotate(this->camRotX, 0, 1, 0);
  cameraTransformation.rotate(this->camRotY, 1, 0, 0);

  QVector3D cameraPosition = cameraTransformation * QVector3D(0,0,this->camDistance);
  QVector3D cameraUpDirection = cameraTransformation * QVector3D(0, 1, 0);

  viewMatrix.lookAt(cameraPosition, QVector3D(0, 0, 0), cameraUpDirection);

  foreach(GLObject *o, this->boden){
    modelMatrix.setToIdentity();
    modelMatrix.translate(o->getPosition()[0],o->getPosition()[1],o->getPosition()[2]);
    modelMatrix.rotate(o->getRotation()[0],1,0,0);
    modelMatrix.rotate(o->getRotation()[1],0,1,0);
    modelMatrix.rotate(o->getRotation()[2],0,0,1);
    modelMatrix.scale(o->getScale()[0], o->getScale()[1],o->getScale()[2]);
    o->render(modelMatrix,perspectiveMatrix,viewMatrix);
  }

  foreach(GLObject *o, this->objects){
    modelMatrix.setToIdentity();
    modelMatrix.translate(o->getPosition()[0],o->getPosition()[1],o->getPosition()[2]);
    modelMatrix.rotate(o->getRotation()[0],1,0,0);
    modelMatrix.rotate(o->getRotation()[1],0,1,0);
    modelMatrix.rotate(o->getRotation()[2],0,0,1);
    modelMatrix.scale(o->getScale()[0], o->getScale()[1],o->getScale()[2]);
    if(o->getName() == "flag"){
      glDisable(GL_CULL_FACE);
    }
    if(o->getName() == "sky"){
      glCullFace(GL_FRONT);
    }
    o->render(modelMatrix,perspectiveMatrix,viewMatrix);
    if(o->getName() == "flag"){
      glEnable(GL_CULL_FACE);
    }
    if(o->getName() == "sky"){
      glCullFace(GL_BACK);
    }
    if(o->getName() == "ball"){
      MyGLWidget::ballPos = Vec3(o->getPosition()[0],o->getPosition()[1],o->getPosition()[2]);
    }
    o->update();
  }

  this->updateFPS(1000);
  this->update();

}

void MyGLWidget::loadObjects(int scene)
{
  Sphere *s;
  Flag *f;
  Model *m;
  if(!this->alreadyLoaded){
    this->normal = new QOpenGLShaderProgram();
    this->normal->addShaderFromSourceFile(QOpenGLShader::Vertex,":/shaders/normal.vert");
    this->normal->addShaderFromSourceFile(QOpenGLShader::Fragment,":/shaders/normal.frag");
    this->normal->link();

    this->default130 = new QOpenGLShaderProgram();
    this->default130->addShaderFromSourceFile(QOpenGLShader::Vertex,":/shaders/default130.vert");
    this->default130->addShaderFromSourceFile(QOpenGLShader::Fragment,":/shaders/default130.frag");
    this->default130->link();

    this->phong = new QOpenGLShaderProgram();
    this->phong->addShaderFromSourceFile(QOpenGLShader::Vertex,":/shaders/phong.vert");
    this->phong->addShaderFromSourceFile(QOpenGLShader::Fragment,":/shaders/phong.frag");
    this->phong->link();
    this->alreadyLoaded = true;

    m = new Model("/home/geoffrey/Documents/FH_Aachen/CG/Abschlussprojekt/simpleplane2.obj");
    for(int i = -10; i < 10 ; i++){
      for(int j = -10; j < 10 ;j++){
        s = new Sphere(m,this->phong,":/textures/sand.jpg","test");
        this->boden.push_back(s);
        s->setPostition(i*2,-2,j*2);
        s->setScale(2,2,2);
      }
    }
  }
  switch (scene) {
    case 1:
      //flagge
      m = new Model("/home/geoffrey/Documents/FH_Aachen/CG/Abschlussprojekt/simpleplane.obj");
      f = new Flag("flag",":/textures/ci_flag.png",this->phong,m,true,1,{0.0,2.0,0.0});
      f->setScale(2.5,2.5,2.5);
      f->fixVert(0);
      f->fixVert(15 * (15 - 1));
      this->objects.push_back(f);

      //fanenmast
      m = new Model("/home/geoffrey/Documents/FH_Aachen/CG/Abschlussprojekt/pole.obj");
      s = new Sphere(m,this->default130,":/textures/grey.jpg","test");
      this->objects.push_back(s);

      //Windrichtungspfeil
      m = new Model("/home/geoffrey/Documents/FH_Aachen/CG/Abschlussprojekt/arrow4.obj");
      s = new Sphere(m,this->normal,":/textures/ci_flag.png","windrichtung");
      s->setPostition(0,6,0);
      this->objects.push_back(s);

      //skybox
      m = new Model("/home/geoffrey/Documents/FH_Aachen/CG/Abschlussprojekt/sphere_low.obj");
      s = new Sphere(m,this->default130,":/textures/sky.jpg","sky");
      s->setPostition(0,-500,0);
      s->setScale(1000,1000,1000);
      this->objects.push_back(s);
      break;
    case 2:
      //flagge
      m = new Model("/home/geoffrey/Documents/FH_Aachen/CG/Abschlussprojekt/simpleplane.obj");
      f = new Flag("flag",":/textures/ci_flag.png",this->phong,m,true,1,{0.0,-.5,0.0});
      f->setScale(2.5,2.5,2.5);
      f->fixVert(15*14);
      f->fixVert(15 * 15 - 1);
      this->objects.push_back(f);

      //skybox
      m = new Model("/home/geoffrey/Documents/FH_Aachen/CG/Abschlussprojekt/sphere_low.obj");
      s = new Sphere(m,this->default130,":/textures/sky.jpg","sky");
      s->setPostition(0,-550,0);
      s->setScale(1000,1000,1000);
      this->objects.push_back(s);

      //ball
      m = new Model("/home/geoffrey/Documents/FH_Aachen/CG/Abschlussprojekt/sphere_low.obj");
      s = new Sphere(m,this->phong,":/textures/football.png","ball");
      s->setPostition(1,0,5);
      this->objects.push_back(s);

      MyGLWidget::windDir = Vec3(-0.0001,-0.001,-0.0001);
      break;
    default:
      //skybox
      m = new Model("/home/geoffrey/Documents/FH_Aachen/CG/Abschlussprojekt/sphere_low.obj");
      s = new Sphere(m,this->default130,":/textures/sky.jpg","sky");
      s->setPostition(0,-550,0);
      s->setScale(1000,1000,1000);
      this->objects.push_back(s);
      break;
    }

}

void MyGLWidget::updateFPS(int ms)
{
  this->frames++;
  if(this->time.elapsed() > ms) {
    double multiplier = 1000.0 /this->time.elapsed();

    this->time.restart();
    this->fps = this->frames* multiplier;
    this->frames = 0;
    emit sendFPS(this->fps);
  }
}

void MyGLWidget::loadScene(int scene)
{
  //delete this scene
  foreach(GLObject *o, this->objects){
    delete(o);
  }
  this->objects.clear();
  //look up which scene && load it
  this->loadObjects(scene);
}

void MyGLWidget::receiveGranu(int value)
{
  foreach(GLObject *o, this->objects){
    if(o->getName() == "flag"){
      Flag *f = (Flag *)o;
      f->granuUp(value);
      switch (this->sceneval) {
        case 1:
          f->fixVert(0);
          f->fixVert(value * (value - 1));
          break;
        case 2:
          f->fixVert(value * (value - 1));
          f->fixVert(value * value - 1);
          break;
        default:
          break;
      }
    }
  }
}

void MyGLWidget::receiveDepth(int value)
{
  foreach(GLObject *o, this->objects){
    if(o->getName() == "flag"){
      Flag *f = (Flag *)o;
      f->setNeighDepth(value);
    }
  }
}

void MyGLWidget::receiveScene(int value)
{
  this->loadScene(value);
  this->sceneval = value;
}

void MyGLWidget::receiveBallPos(int value)
{
  foreach(GLObject *o, this->objects){
    if(o->getName() == "ball"){
      o->setPostition(o->getPosition()[0],o->getPosition()[1],((float)(value))/10.0 - 5);
    }
    }
}

void MyGLWidget::receiveForce()
{
  foreach(GLObject *o, this->objects){
    if(o->getName() == "flag"){
      Flag *f = (Flag *)o;
      f->blow(Vec3(0,-.0001,0), false);
      f->blow(MyGLWidget::windDir,true);
    }
  }
}

void MyGLWidget::wheelEvent(QWheelEvent *event)
{
  if(this->camDistance + event->delta()/50.0f < 0){
    this->camDistance += event->delta()/50.0f;
  }
  else{
    this->camDistance = -0.1;
  }
}

void MyGLWidget::mousePressEvent(QMouseEvent * event)
{
  if(event->button() == Qt::LeftButton){
    this->mousePosX = event->x();
    this->mousePosY = event->y();
    this->mouseLeftPressed = true;
  }
}

void MyGLWidget::mouseReleaseEvent(QMouseEvent *event)
{
  if(event->button() == Qt::LeftButton){
    this->mouseLeftPressed = false;
  }
}

void MyGLWidget::mouseMoveEvent(QMouseEvent *event)
{
  if(this->mouseLeftPressed == true){

    //kamera drehen
    this->camRotX -= (this->mousePosX - event->x());
    //winkel von oben runter gucken && winkel von unten raufschauen
    if(this->camRotY - (this->mousePosY - event->y()) < this->maxCamRot && this->camRotY - (this->mousePosY - event->y()) > -this->maxCamRot){
      this->camRotY -= (this->mousePosY - event->y());
    } else if(this->camRotY - (this->mousePosY - event->y()) < this->maxCamRot){
      this->camRotY = -(this->maxCamRot-1);
    }
    else if (this->camRotY - (this->mousePosY - event->y()) > -this->maxCamRot){
      this->camRotY = (this->maxCamRot-1);
    }
    //maus position neu speichern
    this->mousePosX = event->x();
    this->mousePosY = event->y();

    //um die werte klein zu halten
    if(this->camRotX < 0)
      this->camRotX += 360;
    if(this->camRotX > 360)
      this->camRotX -= 360;
  }
}

void MyGLWidget::keyPressEvent(QKeyEvent *event)
{
  switch(event->key())
  {
    case Qt::Key_W:
      MyGLWidget::windDir = Vec3(sin((this->camRotY + 90) * CONVERSIONDEGRAD) * sin(this->camRotX * CONVERSIONDEGRAD),
                                 cos((this->camRotY + 90) * CONVERSIONDEGRAD),
                                 sin((this->camRotY + 90) * CONVERSIONDEGRAD) * cos(this->camRotX * CONVERSIONDEGRAD))
                                 .normalized() * 0.02;
      std::cout << MyGLWidget::windDir.f[0] << " " << MyGLWidget::windDir.f[2] << std::endl;
      foreach (GLObject *o, this->objects) {
        if(o->getName() == "windrichtung"){
          o->setRotation(0,this->camRotX+90,this->camRotY -180);
        }
      }
      break;
    default:
      QOpenGLWidget::keyPressEvent(event);
      break;
  }
}

void MyGLWidget::onMessageLogged(QOpenGLDebugMessage message) {
  //qDebug() << message;
}
