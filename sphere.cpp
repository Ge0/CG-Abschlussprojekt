#include "sphere.h"

Sphere::Sphere(Model *model, QOpenGLShaderProgram *shader, const QString &texture,
               std::string name, double size, double distx , double disty , double distz) :
  GLObject(name,std::vector<double> {distx,disty,distz},std::vector<double> {0,0,0},
              std::vector<double> {size,size,size}, texture,shader,model)
{

}

void Sphere::update()
{

}
